package com.codingdojo.model;

import com.google.gson.Gson;

public class User {
	
	private int id;
	private String name;
	private String firstName;
	private String email;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
    }
    
    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
    }
    
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Override
    public String toString() {
        return new Gson().toJson(this);
    }	
	
}
package com.codingdojo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

	@GetMapping("/greeting")
	public String greeting() {
		System.out.println("************************************");
		System.out.println("************************************");
		return "Hola ";
	}

}
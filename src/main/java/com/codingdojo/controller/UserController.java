package com.codingdojo.controller;

import java.util.ArrayList;
import java.util.List;

import com.codingdojo.configuration.common.BadRequestException;
import com.codingdojo.model.User;
import com.codingdojo.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/users")
    @ResponseStatus(code = HttpStatus.CREATED)
    public User createUser(
        @RequestBody User newUser) {
        System.out.println("[UserController] - Starting creatUser method");

        if(newUser == null)
            throw new BadRequestException("Request body is required");
            
        if(newUser.getName() == null)
            throw new BadRequestException("User name is required");
            
        User user = this.userService.createUser(newUser);

        System.out.println("[UserController] - Ending creatUser method");
        return user;
    }

    @PutMapping("/users/{userId}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public User updateUser(
        @PathVariable int userId,
        @RequestBody User updateUser) {
        System.out.println("[UserController] - Starting updateUser method");
        
        updateUser.setId(userId);
        User user = this.userService.updateUser(updateUser);

        System.out.println("[UserController] - Ending updateUser method");
        return user;
    }

    @DeleteMapping("/users/{userId}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public String deleteUser(
        @PathVariable int userId){
        System.out.println("[UserController] - Starting deleteUser method");

        this.userService.deleteUser(userId);

        System.out.println("[UserController] - Ending deleteUser method");
        return "Usuario eliminado";
    }

    @GetMapping("/users/{userId}")
    @ResponseStatus(code = HttpStatus.OK)
    public User getUserById(
        @PathVariable int userId) {
        System.out.println("[UserController] - Starting getUserById method");

        User user = this.userService.getUserById(userId);
        
        System.out.println("[UserController] - Ending getUserById method");
        return user;
    }

    @GetMapping("/users")
    @ResponseStatus(code = HttpStatus.OK)
    public List<User> getUsers(){
        List<User> users = new ArrayList<>();
        System.out.println("[UserController] - Starting getUsers method");
        
        users = this.userService.getUsers();
        System.out.println("Users: " + users);
        
        System.out.println("[UserController] - Ending getUsers method");
        return users;
    }

}

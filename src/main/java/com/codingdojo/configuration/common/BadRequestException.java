package com.codingdojo.configuration.common;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8609911255037832273L;
	
	private String status;
	private Map<String, String> mergeVariables;
	
	
	public BadRequestException() {
		super();
	}

	public BadRequestException(String message) {
		super(message);
	}
	
	public BadRequestException(String status, Map<String, String> mergeVariables) {
		this.status = status;
		this.mergeVariables = mergeVariables;
	}

	public String getStatus() {
		return status;
	}

	public Map<String, String> getMergeVariables() {
		return mergeVariables;
	}
}

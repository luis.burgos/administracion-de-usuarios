package com.codingdojo.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@ComponentScan(value = {"com.codingdojo.controller", "com.codingdojo.service", "com.codingdojo.dao"})
@EnableWebMvc
@Import(SpringJdbcConfig.class)
public class ApplicationConfiguration {
	
}
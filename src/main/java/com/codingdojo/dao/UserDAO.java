package com.codingdojo.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.codingdojo.model.User;
import com.mysql.jdbc.Statement;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAO {

    public User createUser(User user) {
        logger.info("[UserDAO] - Starting createUser method");
        
        Statement statement = null;
        ResultSet resultSet = null;
        String sql = "INSERT INTO USER(NAME, FIRSTNAME, EMAIL) VALUES ('" + user.getName() + "', '" + user.getFirstName() + "', '" + user.getEmail() + "');";
        try{
            Connection connection = this.datasource.getConnection();
            statement = (Statement) connection.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
                    java.sql.ResultSet.CONCUR_UPDATABLE);
            statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            resultSet = statement.getGeneratedKeys();
            while (resultSet.next()) {
                user.setId(resultSet.getInt(1));
                logger.info("[UserDAO] - Response: " + user.toString());
            }
            resultSet.close();
        } catch(SQLException e){
            logger.error(e);
        }
        
        logger.info("[UserDAO] - Ending createUser method");
        return user;
    }

    public User updateUser(User user) {
        logger.info("[UserDAO] - Starting updateUser method");

        final String sql = "UPDATE USER SET NAME = '" + user.getName() + "', FIRSTNAME = '" + user.getFirstName() + "', EMAIL = '" + user.getEmail() + "' WHERE ID = "+ user.getId() +";";
        this.jdbcTemplate.update(sql);

        logger.info("[UserDAO] - Starting updateUser method");
        return user;
    }

    public void deleteUser(final int id) {
        logger.info("[UserDAO] - Starting deleteUser method");

        final String sql = "DELETE FROM USER WHERE ID = " + id + ";";
        this.jdbcTemplate.update(sql);

        logger.info("[UserDAO] - Starting deleteUser method");
    }

    public User getUserById(final int id) {
        logger.info("[UserDAO] - Starting getUserById method");

        final String sql = "SELECT * FROM USER WHERE ID = " + id;
        final User user = new User();
        this.jdbcTemplate.query(sql, new RowCallbackHandler(){
            @Override
            public void processRow(final ResultSet rs) throws SQLException {
                user.setId(rs.getInt("ID"));
                user.setName(rs.getString("NAME"));
                user.setFirstName(rs.getString("FIRSTNAME"));
                user.setEmail(rs.getString("EMAIL"));
            }
        });
        logger.info("[UserDAO] - Ending getUserById method");
        return user;
    }

    public List<User> getUsers() {
        logger.info("[UserDAO] - Starting getUsers method");
        
        final String sql = "SELECT * FROM USER";
        final List<User> users = this.jdbcTemplate.query(sql, new RowMapper(){
            @Override
            public User mapRow(final ResultSet rs, final int rowNum) throws SQLException{
                final User user = new User();
                user.setId(rs.getInt("ID"));
                user.setName(rs.getString("NAME"));
                user.setFirstName(rs.getString("FIRSTNAME"));
                user.setEmail(rs.getString("EMAIL"));
                return user;
            }
        });
        
        logger.info("[UserDAO] - Ending getUsers method");
        return users;
    }

    @Autowired
    public void setDataSource(final DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        this.datasource = dataSource;
    }

    private JdbcTemplate jdbcTemplate;
    private DataSource datasource;
    private static final Logger logger = Logger.getLogger(UserDAO.class);

}
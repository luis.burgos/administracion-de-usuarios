package com.codingdojo.service;

import com.codingdojo.dao.UserDAO;
import com.codingdojo.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
	private UserDAO userDAO;

    public User createUser(User user) {
        System.out.println("[UserService] - Starting createUser method");
        
        User newUser = this.userDAO.createUser(user);

        System.out.println("[UserService] - Ending createUser method");
        return newUser;
    }

    public User updateUser(User user) {
        System.out.println("[UserService] - Starting updateUser method");

        User userUpdate = this.userDAO.updateUser(user);

        System.out.println("[UserService] - Ending updateUser method");
        return userUpdate;
    }

    public void deleteUser(int id) {
        System.out.println("[UserService] - Starting deleteUser method");

        this.userDAO.deleteUser(id);

        System.out.println("[UserService] - Ending deleteUser method");
    }

    public User getUserById(Integer id) {
        System.out.println("[UserService] - Starting getUserById method");

        User user = userDAO.getUserById(id);
        // User user = new User();
        
        // user.setName("Test");

        System.out.println("[UserService] - Ending getUserById method");
        
        return user;
    }

    public List<User> getUsers() {
        System.out.println("[UserService] - Starting getUsers method");

        List<User> listUsers= userDAO.getUsers();

        System.out.println("[UserService] - Ending getUsers method");
		return listUsers;
    }
    
}
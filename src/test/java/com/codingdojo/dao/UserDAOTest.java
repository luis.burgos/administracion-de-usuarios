package com.codingdojo.dao;

import java.util.List;

import com.codingdojo.configuration.ApplicationConfiguration;
import com.codingdojo.model.User;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class, classes = { ApplicationConfiguration.class })
public class UserDAOTest {
    
    // @Test
    // public void caseCreateUserTest(){
    //     logger.info("[UserDAOTest] - START: createUserTest test");

    //     logger.info("[UserDAOTest] - End: createUserTest test");
    // }

    @Test
    public void createUserTest() {
        logger.info("[UserDAOTest] - START: createUserTest test");

        User newUser = new User();
        newUser.setName("Tania");
        newUser.setFirstName("Vivanco");
        newUser.setEmail("tania.vivanco@inteec.la");
        User userTest = this.userDAO.createUser(newUser);
        if(userTest != null)
            logger.info("[UserDAOTest] - Response: " + userTest.toString());

        Assert.assertNotNull(userTest);
        
        logger.info("[UserDAOTest] - END: createUserTest test");
    }

    @Test
    public void updateUserTest(){
        logger.info("[UserDAOTest] - START: updateUserTest test");
        
        User updateUser = new User();
        updateUser.setId(1);
        updateUser.setName("Tania");
        updateUser.setFirstName("Vivanco");
        updateUser.setEmail("tania.vivanco@inteec.la");
        this.userDAO.updateUser(updateUser);

        logger.info("[UserDAOTest] - END: updateUserTest test");
    }

    @Test
    public void deleteUserTest(){
        logger.info("[UserDAOTest] - START: deleteUserTest method");

        this.userDAO.deleteUser(11);

        logger.info("[UserDAOTest] - END: deleteUserTest method");
    }

    @Test
    public void getUserByIdTest(){
        logger.info("[UserDAOTest] - START: getUserByIdTest test");

        int userId = 10;
        User userTest = this.userDAO.getUserById(userId);
        if(userTest != null)
            logger.info("[UserDAOTest] - Response: " + userTest.toString());

        Assert.assertNotNull(userTest);
        
        logger.info("[UserDAOTest] - END: getUserByIdTest test");
    }

    @Test
    public void getUsersTest(){
        logger.info("[UserDAOTest] - START: getUsers method");

        List<User> users = this.userDAO.getUsers();

        logger.info("[UserDAOTest] - Response: " + users);
        Assert.assertNotNull(users);

        logger.info("[UserDAOTest] - END: getUsers method");
    }

    @Autowired
    private UserDAO userDAO;

    private static final Logger logger = Logger.getLogger(UserDAOTest.class);

}

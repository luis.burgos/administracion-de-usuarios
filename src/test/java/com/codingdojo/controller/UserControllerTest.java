package com.codingdojo.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.codingdojo.configuration.ApplicationConfiguration;
import com.codingdojo.model.User;
import com.codingdojo.service.UserService;
import com.google.gson.Gson;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = { ApplicationConfiguration.class })
public class UserControllerTest {
	
	@Before
	public void beforeEachTestConfiguration() {
		g = new Gson();
		
		MockitoAnnotations.initMocks(this);
		
		this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
	}

	@Test
	public void getAllUsersTest() throws Exception {
		logger.info("START: getAllUsersTest test");
		
		this.mockMvc.perform(get("/users")).andExpect(status().isOk());
		
		logger.info("END: getAllUsersTest test");
	}
	
	@Test
	public void createUserCreatedTest() throws Exception{
		logger.info("START: createUserCreatedTest test");
		String user;
		User userVO = new User();
		
		userVO.setName("Test user");
		
		user = userVO.toString();
		
		this.mockMvc.perform(post("/users")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(user))
		.andExpect(status().isCreated());
		
		logger.info("END: createUserCreatedTest test");
	}
	
	@Test
	public void createUserBadRequestTest() throws Exception{
		logger.info("START: createUserBadRequestTest test");
		
		String user;
		User userVO = new User();
		
		user = userVO.toString();
		
		this.mockMvc.perform(post("/users")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(user))
		.andExpect(status().isBadRequest());
		
		logger.info("END: createUserBadRequestTest test");
	}
	
	@Test
	public void getUserTest() throws Exception{
		logger.info("START: getUserTest test");
		
		User userMock = new User();
		
		userMock.setName("User");
		userMock.setFirstName("Testing");
		userMock.setEmail("user.testing@inteec.la");
		
		Mockito.when(userService.getUserById(Mockito.any())).thenReturn(userMock);
		
		MvcResult result = this.mockMvc.perform(get("/users/1")
				.contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
		
		MockHttpServletResponse response = result.getResponse();
		
		logger.debug("Response status: " + response.getStatus());
		
		Assert.assertEquals(HttpStatus.OK.value(), response.getStatus());
		
		User userResponse = g.fromJson(response.getContentAsString(), User.class);
		
		logger.debug("user response: " + userResponse);
		
		Assert.assertEquals(userMock.getName(), userResponse.getName());
		Assert.assertEquals(userMock.getFirstName(), userResponse.getFirstName());
		Assert.assertEquals(userMock.getEmail(), userResponse.getEmail());
		Assert.assertEquals(userMock.getId(), userResponse.getId());
		
		logger.info("END: getUserTest test");
	}
	
	private static final Logger logger = Logger.getLogger(UserControllerTest.class);
	
	@Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;
	
	private MockMvc mockMvc;
	
	private Gson g;
}
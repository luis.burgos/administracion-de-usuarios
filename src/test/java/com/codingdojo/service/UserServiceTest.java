package com.codingdojo.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.codingdojo.configuration.ApplicationConfiguration;
import com.codingdojo.dao.UserDAO;
import com.codingdojo.model.User;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = { ApplicationConfiguration.class })
public class UserServiceTest {
	
	@Before
	public void beforeEachTestConfiguration() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getUsersTest() {
		logger.info("START SERVICE TEST: getUserTest test");
		List<User> userList;
		List<User> userMockList = this.getUserListMock();
		
		Mockito.when(userDAO.getUsers()).thenReturn(userMockList);
		
		userList = this.userService.getUsers();
		
		Assert.assertNotNull(userList);
		
		logger.debug("User mock list obtained from DAO: " + userList.toString());
		
		Assert.assertEquals(userList.size(), 3);
		
		User userMock = userList.get(1);
		
		Assert.assertNotNull(userMock.getId());
		Assert.assertEquals(userMock.getId(), 2);
		
		Assert.assertNotNull(userMock.getName());
		Assert.assertEquals(userMock.getName(), "Mauricio");
		
		Assert.assertNotNull(userMock.getFirstName());
		Assert.assertEquals(userMock.getFirstName(), "Damian");
		
		Assert.assertNotNull(userMock.getEmail());
		Assert.assertEquals(userMock.getEmail(), "mauricio.damian@inteec.la");
		
		logger.debug("User obtained from response mock list: " + userMock.toString());
		
		logger.info("END SERVICE TEST: getUserTest test");
	}
	
	@Test
	public void getUsersEmptyResultTest() {
		logger.info("START SERVICE TEST: getUsersEmptyResultTest test");
		List<User> userList;
		List<User> userMockList = new ArrayList<>();
		
		Mockito.when(userDAO.getUsers()).thenReturn(userMockList);
		
		userList = this.userService.getUsers();
		
		Assert.assertNotNull(userList);
		
		logger.debug("User mock list obtained from DAO: " + userList.toString());
		
		Assert.assertEquals(userList.size(), 0);
		
		logger.info("END SERVICE TEST: getUsersEmptyResultTest test");
	}
	
	private List<User> getUserListMock(){
		List<User> userMockList = new ArrayList<>();
		
		User userMock1 = new User();
		User userMock2 = new User();
		User userMock3 = new User();
		
		userMock1.setId(1);
		userMock1.setName("Luis");
		userMock1.setFirstName("Burgos");
		userMock1.setEmail("luis.burgos@inteec.la");
		
		userMock2.setId(2);
		userMock2.setName("Mauricio");
		userMock2.setFirstName("Damian");
		userMock2.setEmail("mauricio.damian@inteec.la");
		
		userMock3.setId(3);
		userMock3.setName("Rafael");
		userMock3.setFirstName("Rosas");
		userMock3.setEmail("rafael.santos@inteec.la");
		
		userMockList.add(userMock1);
		userMockList.add(userMock2);
		userMockList.add(userMock3);
		
		return userMockList;
	}
	
	private static final Logger logger = Logger.getLogger(UserServiceTest.class);
	
	@Mock
    private UserDAO userDAO;
	
	@InjectMocks
	private UserService userService;
}